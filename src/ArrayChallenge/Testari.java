package ArrayChallenge;

import java.util.Arrays;
import java.util.Scanner;

public class Testari {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] myIntegers = getIntegers(4);
        printArray(myIntegers);
        sortIntegers(myIntegers);
        System.out.println("Sorted: ");
        int[] sorted = sortIntegers(myIntegers);
        printArray(sorted);

    }

    public static int[] getIntegers(int capacity ){
        System.out.println("Enter " + capacity+ " integer values.\r");
        int [] array = new int[capacity];
        for(int i=0;i<array.length;i++){
            array[i] = scanner.nextInt();
        }
        return array;
    }

    public static void printArray(int[]array){
        for(int i=0; i<array.length; i++){
            System.out.println("Element "+ i+" contents "+array[i]);
        }
    }

    public static int[] sortIntegers(int []arrray){
//        int []sortedArray = int[arrray.length];
//        for(int i=0; i<arrray.length;i++){
//            sorted[i] = arrray[i];
//        }
        int []sortedArray = Arrays.copyOf(arrray,arrray.length);

        boolean flag = true;
        int temp;

        while(flag){
            flag = false;
            for(int i=0; i<sortedArray.length-1; i++){
                if(sortedArray[i]<sortedArray[i+1]){
                    temp = sortedArray[i];
                    sortedArray[i] = sortedArray[i+1];
                    sortedArray[i+1] = temp;
                    flag = true;
                }
            }
        }return sortedArray;



    }



}
