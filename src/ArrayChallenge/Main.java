package ArrayChallenge;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        Integer[] myIntegers = getIntegers(5);
        Integer[] sorted = sortIntegers(myIntegers);

        printArray(sorted);


    }

    public static Integer[] getIntegers(Integer capacity){
        System.out.println("Enter "+capacity+" integer values\r");
        Integer[]array = new Integer[capacity];
        for(int i=0; i<array.length; i++){
            array[i] =scanner.nextInt();
        }
        return array;
    }

    public static void printArray(Integer []array){
        for (int i=0; i<array.length; i++){
            System.out.println("Element "+i+" contents "+array[i]);
        }
    }

    public static Integer[] sortIntegers(Integer []array){

        Arrays.sort(array,Collections.reverseOrder());
//        int[]sortedArray = new int[array.length];
//        for(int i=0; i<array.length;i++){
//            sortedArray[i] = array[i];
//        }
//        int[]sortedArray = Arrays.copyOf(array,array.length);//cream un nou array sorted sa fie egal cu cel ca parametru
//                                                             //este = cu ce am comentat
//        boolean flag = true;
//        int temp;
//        while(flag){
//            flag = false;
//            for(int i=0; i<sortedArray.length-1; i++){
//                if(sortedArray[i]<sortedArray[i+1]){
//                    temp = sortedArray[i];
//                    sortedArray[i] = sortedArray[i+1];
//                    sortedArray[i+1] = temp;
//                    flag = true;
//
//                }
//            }
//        }return sortedArray;
        return array;
    }

}
