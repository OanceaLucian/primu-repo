package Arrays;

import java.util.Arrays;

public class TestariPracticeIt {
    public static void main(String[] args) {
        int[] list1 = {45,20,300};
        int[] list2 = {50,41,644};
        //var 1 -folosim functia :)  System.out.println(Arrays.equals(array1,array2));
        //var 2: facem noi functia
        System.out.println(allLess(list1, list2));

    }
    public static boolean allLess(int[] list1, int[] list2) {
        if (list1.length != list2.length) {
            return false;
        }else {
            for (int i = 0; i < list1.length; i++) {
                if (list1[i] >= list2[i]) {
                    return false;
                }
            }
        }
        return true;
    }

//    public static boolean equals(String[] arrayP1, String[] arrayP2) {
//        boolean equalOrNot = true;
//        if (arrayP1.length == arrayP2.length) {
//            for (int i = 0; i < arrayP1.length; i++) {
//                if (arrayP1[i] != arrayP2[i]) {
//                    equalOrNot = false;
//                }
//            }
//        } else {
//            equalOrNot = false;
//        }
//        return equalOrNot;
//    }
}