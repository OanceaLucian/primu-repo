package ReverseArrayChallenge;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int [] array = {1,5,3,7,11,9,15};
        System.out.println("Array = "+ Arrays.toString(array));
        reverse(array);
        System.out.println("Reversed array = "+Arrays.toString(array));


    }


    private static void reverse(int[]array) {
        int maxIndex = array.length - 1;
        int halfLength = array.length / 2;

        for (int i = 0; i < halfLength; i++) {
            int temp = array[i];
            array[i] = array[maxIndex - i];
            array[maxIndex - i] = temp;
        }
    }
//// var 2:
////            for(int i = 0; i < array.length / 2; i++)
////            {
////                int temp = array[i];
////                array[i] = array[array.length - i - 1];
////                array[array.length - i - 1] = temp;
////            }
//
//
//

//Varianta 3 : cu while
   // public static void reverse(int[] array) {
//        int left = 0;
//        int right = array.length - 1;
//
//        while( left < right ) {
//            // swap the values at the left and right indices
//            int temp = array[left];
//            array[left] = array[right];
//            array[right] = temp;
//
//            // move the left and right index pointers in toward the center
//            left++;
//            right--;
//        }
//    }
}
